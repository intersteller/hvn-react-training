// khai bao constant name de tranh trung lap khi goi action hoac reducer
export const BLOG_START = "BLOG_START";
export const BLOG_ERROR = "BLOG_ERROR";
export const BLOG_SUCCESS = "BLOG_SUCCESS";
export const BLOG_ERROR_START = "BLOG_ERROR_START";
export const GET_POST_START = "GET_POST_START";
export const GET_POST_SUCCESS = "GET_POST_SUCCESS";
export const GET_POST_ERROR = "GET_POST_ERROR";
export const DELETE_POST_START = "DELETE_POST_START";
export const DELETE_POST_SUCCESS = "DELETE_POST_SUCCESS";
export const DELETE_POST_ERROR = "DELETE_POST_ERROR";
