import { put, takeLatest, all } from 'redux-saga/effects';

import * as types from "../actions/ActionTypes";

function * getBlogData() {
  // data moi se update vao Redux thong qua Saga
  // co the thay doi mock data ben duoi bang request den API voi async/await
  let data = yield fetch("https://jsonplaceholder.typicode.com/posts");
  data = yield data.json();

  if (data.length > 0) {
    // truyen data den reducer voi type BLOG_SUCCESS
    yield put({type: types.BLOG_SUCCESS, payload: data});
  } else {
    // truyen data den reducer voi type BLOG_ERROR
    yield put({type: types.BLOG_ERROR});
  }
}

function * getBlogDataError() {
  // gia su data request loi
  // const data = [];
  // truyen message den reducer voi type BLOG_ERROR
  yield put({type: types.BLOG_ERROR, payload: "Data is empty!"});
}

function * getPostData(action) {
  const id = action.id;
  let data = yield fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
  data = yield data.json();
  if (data) {
    yield put({type: types.GET_POST_SUCCESS, payload: data});
  } else {
    yield put({type: types.GET_POST_ERROR, payload: "Data is empty!"});
  }
}

function * deletePost(action) {
  const id = action.id;
  let data = yield fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, { method: "DELETE" });
  data = yield data.json();
  if (data) {
    yield put({ type: types.DELETE_POST_SUCCESS });
  } else {
    yield put({ type: types.DELETE_POST_ERROR });
  }
}

// lang nghe action va goi Saga function
function * actionBlogWatcher() {
  yield takeLatest(types.BLOG_START, getBlogData);
}

function * actionBlogErrorWatcher() {
  yield takeLatest(types.BLOG_ERROR_START, getBlogDataError);
}

function * actionPostWatcher() {
  yield takeLatest(types.GET_POST_START, getPostData);
}

function * actionPostDeleteWatcher() {
  yield takeLatest(types.DELETE_POST_START, deletePost);
}

// chua tat ca cac action can phai lang nghe
export default function * root() {
  yield all([
    actionBlogWatcher(),
    actionBlogErrorWatcher(),
    actionPostWatcher(),
    actionPostDeleteWatcher()
  ])
}
