import { combineReducers } from "redux";

import Blog from "./Blog";
import Post from "./Post";

// chua tat ca cac reducers
const rootReducer = combineReducers({
  Blog,
  Post
});

export default rootReducer;
