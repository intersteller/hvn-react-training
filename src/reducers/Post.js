import * as types from "../actions/ActionTypes";

// khai bao default state object se luu trong redux store
const initialState = {
  data: {},
  message: null
};

// khai bao cac reducer se su dung
// reducer se luu data lay duoc tu saga vao redux store
// moi action type thuong se duoc luu vao 1 object rieng
const PostReducer = (state = initialState, action) => {
  switch (action.type) {
    // update data nhan duoc tu saga vao initialState
    case types.GET_POST_SUCCESS:
      return {
        ...state,
        data: action.payload,
        message: null
      };
    case types.GET_POST_ERROR:
      return {
        ...state,
        data: [],
        message: action.payload
      };
    case types.DELETE_POST_SUCCESS:
      return {
        ...state,
        data: {},
        message: "This post is removed."
      };

    default:
      return state;
  }
};

export default PostReducer;
