import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware, { END } from "redux-saga";
import { createBrowserHistory } from "history";

import rootReducers from "../reducers";
import rootSagas from "../sagas";

export default () => {
  // Create a history depending on the environment
  const history = createBrowserHistory();

  const enhancers = [];

  // Dev tools are helpful
  if (process.env.NODE_ENV === "development") {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

    if (typeof devToolsExtension === "function") {
      enhancers.push(devToolsExtension());
    }
  }

  const sagaMiddleware = createSagaMiddleware();
  const middleware = [sagaMiddleware];
  const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
  );

  // Do we have preloaded state available? Great, save it.
  const initialState = window.__PRELOADED_STATE__;
  delete window.__PRELOADED_STATE__;

  // Create the store
  const store = createStore(
    rootReducers,
    initialState,
    composedEnhancers
  );

  let sagaTask = sagaMiddleware.run(function * () {
    yield rootSagas()
  });

  if (process.env.NODE_ENV === "development" && module.hot) {
    module.hot.accept("../reducers", () => {
      const nextRootReducer = require("../reducers").default;
      store.replaceReducer(nextRootReducer);
    });

    module.hot.accept("../sagas", () => {
      const newSagas = require("../sagas").default;
      sagaTask.cancel();
      sagaTask.done.then(() => {
        sagaTask = sagaMiddleware.run(function * replacedSaga() {
          yield newSagas();
        })
      })
    });
  }

  store.close = () => store.dispatch(END);

  return {
    store,
    history
  };
};
